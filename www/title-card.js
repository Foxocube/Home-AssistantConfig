class TitleCard extends HTMLElement {
  set hass(hass) {
    if (!this.card) {
      this.card = document.createElement('ha-card');
      this.card.header = 'Example card';
      this.card.style.textAlign = "center";
      this.appendChild(this.card);
    }

//    if (this.card.shadowRoot.firstElementChild) {
//      if(this.config.mergeDown) {
//        this.card.shadowRoot.firstElementChild.style.padding = "16px 16px 0px 16px"
//      } else {
//        this.card.shadowRoot.firstElementChild.style.padding = "16px"
//      }
//    }

    this.card.header = this.config.title;
  }

  setConfig(config) {
    if (!config.title) {
      throw new Error('You need to define a title');
    }
    this.config = config;
  }

  // The height of your card. Home Assistant uses this to automatically
  // distribute all cards over the available columns.
  getCardSize() {
    return 1;
  }
}

customElements.define('title-card', TitleCard);
