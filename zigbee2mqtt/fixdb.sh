#!/bin/bash
Directory="/var/lib/homeassistant/Home-AssistantConfig/zigbee2mqtt"
LiveFile="$Directory/database.db"
BackupFile="$Directory/database.db.backup"

if((`stat -c%s "$LiveFile"` > 10000));then
    # Live file is bigger than backup, probably working

    # Update the backup
    cp $LiveFile $BackupFile

else
    # Life file is smaller, probably broken

    # Stop zigbee2mqtt
    docker stop zigbee2mqtt

    # Replace the DB database file with the backup
    cp $BackupFile $LiveFile

    # Restart zigbee2mqtt
    /usr/local/bin/fixTasmota
    docker start zigbee2mqtt

fi


